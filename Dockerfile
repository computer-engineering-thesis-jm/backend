FROM openjdk:8-jdk-alpine

WORKDIR api

ADD target/*all.jar backend.jar
ADD metrics/jmxterm-1.0.0-uber.jar jmxterm-1.0.0-uber.jar

EXPOSE 8080

ENTRYPOINT java -jar \
	-Dvertx.options.maxEventLoopExecuteTime=30000000000 \
	-Dvertx.disableDnsResolver=true \
	-Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.SLF4JLogDelegateFactory \
	-Dvertx.metrics.options.enabled=true \
	-Dvertx.metrics.options.registryName=tesis.moreno.metrics \
	-Dvertx.metrics.options.jmxEnabled=true \
	-Dvertx.metrics.options.jmxDomain=vertx \
	-Dcom.sun.management.jmxremote \
	-Dcom.sun.management.jmxremote.local.only=false \
	-Dcom.sun.management.jmxremote.port=7199 \
	-Dcom.sun.management.jmxremote.rmi.port=7199 \
	-Dcom.sun.management.jmxremote.authenticate=false \
	-Dcom.sun.management.jmxremote.ssl=false \
	backend.jar