package com.moreno.backend.server;

import static io.prometheus.client.CollectorRegistry.defaultRegistry;

import com.codahale.metrics.MetricRegistry;
import com.moreno.backend.InstanceCache;
import com.moreno.backend.domain.action.ActionsProvider;
import com.moreno.backend.domain.util.Configuration;
import com.moreno.backend.server.rest.resource.ResourcesProvider;

import io.prometheus.client.dropwizard.DropwizardExports;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

public class ApiVerticle extends AbstractVerticle {

    private static final Logger logger = LoggerFactory.getLogger(ApiVerticle.class);

    private static final String METRICS_RESOURCE = "/metrics";
    private static final String USER_LOGIN_RESOURCE = "/user/login";
    private static final String REGISTER_METRIC_RESOURCE = "/register/metric";

    private final Configuration configuration;

    public ApiVerticle() {
        configuration = InstanceCache.getInstanceFor(Configuration.class, Configuration::new);
    }

    @Override
    public void start() {
        logger.info("Starting");
        InstanceCache.getInstanceFor(Vertx.class, this::getVertx);

        MetricRegistry metricRegistry = ActionsProvider.getMetricRegistry();
        defaultRegistry.register(new DropwizardExports(metricRegistry));

        HttpServerOptions httpServerOptions = new HttpServerOptions()
                .setTcpKeepAlive(false)
                .setReuseAddress(false)
                .setIdleTimeout(300);

        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.post(USER_LOGIN_RESOURCE).handler(ResourcesProvider.provideLoginResource(configuration.getAuthorizationKey()));
        router.post(REGISTER_METRIC_RESOURCE).handler(ResourcesProvider.providePutMetricResource());
        router.get(METRICS_RESOURCE).handler(ResourcesProvider.provideMetricsResource());

        vertx.createHttpServer(httpServerOptions)
             .requestHandler(router::accept)
             .exceptionHandler(event -> logger.error("An unexpected error occurred", event))
             .listen(configuration.getApiPort());
    }
}
