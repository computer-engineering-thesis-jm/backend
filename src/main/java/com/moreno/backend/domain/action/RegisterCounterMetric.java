package com.moreno.backend.domain.action;

import com.moreno.backend.domain.model.Metric;
import com.moreno.backend.infraestructure.metrics.MetricService;

public class RegisterCounterMetric implements RegisterMetricAction {

    private final MetricService metricService;

    public RegisterCounterMetric(MetricService metricService) {
        this.metricService = metricService;
    }

    @Override
    public void execute(Metric metric) {
        metricService.counter(metric);
    }
}
