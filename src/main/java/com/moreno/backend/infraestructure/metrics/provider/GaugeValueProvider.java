package com.moreno.backend.infraestructure.metrics.provider;

public interface GaugeValueProvider<T> {

    T get(String name);
}
