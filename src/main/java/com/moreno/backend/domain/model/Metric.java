package com.moreno.backend.domain.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Metric {

    private final String deviceId;
    private final String name;
    private final Integer value;

    public Metric(String deviceId, String name, Integer value) {
        this.deviceId = deviceId;
        this.name = name;
        this.value = value;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    public enum Type {
        COUNTER,
        GAUGE
    }
}
