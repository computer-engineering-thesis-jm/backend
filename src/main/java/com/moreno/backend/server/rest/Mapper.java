package com.moreno.backend.server.rest;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.lang3.SerializationException;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Mapper {
    private ObjectMapper mapper;

    public Mapper() {
        this.mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    public <T> Map<String, Object> toMap(T object) {
        return mapper.convertValue(object, new TypeReference<Map<String, Object>>() {
        });
    }

    public <T> T fromMap(Map<String, Object> object, Class<T> clazz) {
        return mapper.convertValue(object, clazz);
    }

    public <T> String serialize(T entity) {
        try {
            return mapper.writeValueAsString(entity);
        } catch (JsonProcessingException e) {
            throw new SerializationException(e);
        }
    }

    public <T> T deserialize(String item, Class<T> clazz) {
        try {
            return mapper.readValue(item, clazz);
        } catch (IOException e) {
            throw new SerializationException(e);
        }
    }
}

