package com.moreno.backend.domain.action;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.SharedMetricRegistries;
import com.moreno.backend.InstanceCache;
import com.moreno.backend.domain.MetricRepository;
import com.moreno.backend.infraestructure.DeviceRepository;
import com.moreno.backend.infraestructure.metrics.MetricService;

public class ActionsProvider {

    public static AuthorizeDevice getAuthorizeDevice(String authorization) {
        return InstanceCache.getInstanceFor(AuthorizeDevice.class, () -> new AuthorizeDevice(authorization, getDeviceRepository()));
    }

    public static RegisterMetric getRegisterMetric() {
        return InstanceCache.getInstanceFor(RegisterMetric.class,
                () -> new RegisterMetric(new RegisterGaugeMetric(getMetricService()), new RegisterCounterMetric(getMetricService())));
    }

    private static MetricService getMetricService() {
        return InstanceCache.getInstanceFor(MetricService.class,
                () -> new MetricService(new MetricRepository(), getMetricRegistry()));
    }

    public static MetricRegistry getMetricRegistry() {
        return InstanceCache.getInstanceFor(MetricRegistry.class, () -> SharedMetricRegistries.getOrCreate("tesis.moreno.metrics"));
    }

    private static DeviceRepository getDeviceRepository() {
        return new DeviceRepository();
    }
}
