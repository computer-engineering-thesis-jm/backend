package com.moreno.backend.infraestructure;

import java.util.HashMap;
import java.util.Map;

import com.moreno.backend.domain.model.Device;

import io.reactivex.Maybe;

public class DeviceRepository {

    private final Map<Device, String> map = new HashMap<Device, String>() {{
        put(new Device("device1", "sol1234"), "JsiqNN12-oKn1-nLJNqj2n-9M1k-ONoqwioH");
    }};

    public Maybe<String> get(String deviceId, String password) {
        return Maybe.create(emitter -> {
            Device device = new Device(deviceId, password);
            if(map.containsKey(device)) {
                emitter.onSuccess(map.get(device));
            } else {
                emitter.onComplete();
            }
        });
    }
}
