package com.moreno.backend.domain.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class AuthorizedDevice {

    private final String deviceId;
    private final String password;
    private final String token;

    public AuthorizedDevice(String deviceId, String password, String token) {
        this.deviceId = deviceId;
        this.password = password;
        this.token = token;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getPassword() {
        return password;
    }

    public String getToken() {
        return token;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
