package com.moreno.api.server.rest.resource;

import static com.moreno.backend.server.rest.resource.LoginResource.ErrorDTO;

import java.util.Map;

import org.assertj.core.util.Maps;
import org.junit.Assert;
import org.junit.Test;

import com.moreno.api.server.rest.resource.utils.MockedHttpServerResponse;
import com.moreno.api.server.rest.resource.utils.MockedRoutingContext;
import com.moreno.backend.domain.action.AuthorizeDevice;
import com.moreno.backend.domain.model.AuthorizedDevice;
import com.moreno.backend.infraestructure.DeviceRepository;
import com.moreno.backend.server.rest.Mapper;
import com.moreno.backend.server.rest.ResourceResponse;
import com.moreno.backend.server.rest.resource.LoginResource;
import com.moreno.backend.server.rest.resource.LoginResource.LoginDto;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;

public class LoginResourceTest {

    @Test
    public void testFailureResponseWhenTryToAuthorizeAnInvalidDevice() {
        LoginDto dto = new LoginDto("device2", "invalidPassword");
        DeviceRepository deviceRepository = new DeviceRepository();
        AuthorizeDevice authorizeDevice = new AuthorizeDevice("validAuth", deviceRepository);
        Mapper mapper = new Mapper();
        LoginResource resource = new LoginResource(mapper, authorizeDevice);

        HttpServerResponse response = new MockedHttpServerResponse();
        Map<String, String> headers = Maps.newHashMap("Authorization", "invalidAuth");
        RoutingContext routingContext = new MockedRoutingContext(mapper.serialize(dto), headers, response);

        resource.handle(routingContext);

        String message = "Device " + dto.getDeviceId() + " or Password " + dto.getPassword() + " are invalids.";
        String failureResponse = ResourceResponse.createFailureResponse(new ErrorDTO(message));
        String actualResponse = routingContext.response().getStatusMessage();
        Assert.assertEquals(failureResponse, actualResponse);
    }

    @Test
    public void testAuthorizeDevice() {
        String auth = "1234";
        LoginDto dto = new LoginDto("device1", "sol1234");
        DeviceRepository deviceRepository = new DeviceRepository();
        Mapper mapper = new Mapper();
        AuthorizeDevice authorizeDevice = new AuthorizeDevice(auth, deviceRepository);
        LoginResource resource = new LoginResource(mapper, authorizeDevice);

        HttpServerResponse response = new MockedHttpServerResponse();
        Map<String, String> headers = Maps.newHashMap("Authorization", auth);
        RoutingContext routingContext = new MockedRoutingContext(mapper.serialize(dto), headers, response);

        resource.handle(routingContext);

        String token = deviceRepository.get(dto.getDeviceId(), dto.getPassword()).blockingGet();
        AuthorizedDevice authorizedDevice = new AuthorizedDevice(dto.getDeviceId(), dto.getPassword(), token);
        String successResponse = ResourceResponse.createSuccessResponse(authorizedDevice);

        String actualResponse = routingContext.response().getStatusMessage();
        Assert.assertEquals(successResponse, actualResponse);
    }

    @Test
    public void testFailureResponseWhenTryToAuthorizeDeviceWithInvalidPassword() {
        String auth = "1234";
        LoginDto dto = new LoginDto("device1", "invalidPassword");
        DeviceRepository deviceRepository = new DeviceRepository();
        Mapper mapper = new Mapper();
        AuthorizeDevice authorizeDevice = new AuthorizeDevice(auth, deviceRepository);
        LoginResource resource = new LoginResource(mapper, authorizeDevice);

        HttpServerResponse response = new MockedHttpServerResponse();
        Map<String, String> headers = Maps.newHashMap("Authorization", auth);
        RoutingContext routingContext = new MockedRoutingContext(mapper.serialize(dto), headers, response);

        resource.handle(routingContext);

        String message = "Device " + dto.getDeviceId() + " or Password " + dto.getPassword() + " are invalids.";
        String failureResponse = ResourceResponse.createFailureResponse(new ErrorDTO(message));

        String actualResponse = routingContext.response().getStatusMessage();
        Assert.assertEquals(failureResponse, actualResponse);
    }
}