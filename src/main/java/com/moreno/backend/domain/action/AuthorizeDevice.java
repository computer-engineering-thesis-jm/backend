package com.moreno.backend.domain.action;

import com.moreno.backend.domain.model.AuthorizedDevice;
import com.moreno.backend.infraestructure.DeviceRepository;

import io.reactivex.Maybe;
import io.reactivex.Single;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class AuthorizeDevice {

    private static final Logger logger = LoggerFactory.getLogger(AuthorizeDevice.class);

    private final String authorization;
    private final DeviceRepository deviceRepository;

    public AuthorizeDevice(String authorization, DeviceRepository deviceRepository) {
        this.authorization = authorization;
        this.deviceRepository = deviceRepository;
    }

    public Single<AuthorizedDevice> execute(String key, String deviceId, String password) {
        return Single.fromCallable(() -> key)
                     .filter(authorization::equals)
                     .flatMap(s -> deviceRepository.get(deviceId, password))
                     .map(token -> new AuthorizedDevice(deviceId, password, token))
                     .switchIfEmpty(throwInvalidDeviceException(deviceId, password))
                     .toSingle();
    }

    private Maybe<AuthorizedDevice> throwInvalidDeviceException(String deviceId, String password) {
        return Maybe.error(new InvalidDeviceException("Device " + deviceId + " or Password " + password + " are invalids."));
    }

    private class InvalidDeviceException extends RuntimeException {
        InvalidDeviceException(String message) {
            super(message);
        }
    }
}
