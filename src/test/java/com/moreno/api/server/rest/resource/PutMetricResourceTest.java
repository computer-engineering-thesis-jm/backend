package com.moreno.api.server.rest.resource;

import static com.moreno.backend.domain.model.Metric.Type;
import static com.moreno.backend.server.rest.resource.PutMetricResource.MetricDto;

import java.util.Collections;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.SharedMetricRegistries;
import com.moreno.backend.domain.MetricRepository;
import com.moreno.backend.domain.action.RegisterCounterMetric;
import com.moreno.backend.domain.action.RegisterGaugeMetric;
import com.moreno.backend.domain.action.RegisterMetric;
import com.moreno.backend.domain.model.Metric;
import com.moreno.backend.infraestructure.metrics.MetricService;
import com.moreno.backend.server.rest.Mapper;
import com.moreno.api.server.rest.resource.utils.MockedHttpServerResponse;
import com.moreno.api.server.rest.resource.utils.MockedRoutingContext;
import com.moreno.backend.server.rest.resource.PutMetricResource;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;

public class PutMetricResourceTest {

    private Mapper mapper;
    private PutMetricResource resource;
    private MetricRegistry registry;
    private MetricRepository metricRepository;

    @Before
    public void setUp() {
        mapper = new Mapper();
        metricRepository = new MetricRepository();
        registry = SharedMetricRegistries.getOrCreate(UUID.randomUUID().toString());
        MetricService metricService = new MetricService(metricRepository, registry);
        RegisterGaugeMetric registerGaugeMetric = new RegisterGaugeMetric(metricService);
        RegisterCounterMetric registerCounterMetric = new RegisterCounterMetric(metricService);
        RegisterMetric registerMetric = new RegisterMetric(registerGaugeMetric, registerCounterMetric);
        resource = new PutMetricResource(mapper, registerMetric);
    }

    @Test
    public void testOnHandleGaugeMetricRegisterOnMetricService() {
        MetricDto dto = new MetricDto("aDeviceId", "aName", 123, Type.GAUGE);

        HttpServerResponse response = new MockedHttpServerResponse();
        RoutingContext routingContext = new MockedRoutingContext(mapper.serialize(dto), Collections.emptyMap(), response);

        resource.handle(routingContext);

        Metric metric = new Metric(dto.getDeviceId(), dto.getName(), dto.getValue());
        Assert.assertTrue(metricRepository.contains(metric));

        String successResponse = "{\"success\":true,\"content\":\"{\\\"response\\\":true}\"}";
        String actualResponse = routingContext.response().getStatusMessage();
        Assert.assertEquals(successResponse, actualResponse);
    }

    @Test
    public void testOnHandleGaugeMetricStoreOnMetricRepository() {
        MetricDto dto = new MetricDto("aDeviceId", "aName", 123, Type.GAUGE);

        HttpServerResponse response = new MockedHttpServerResponse();
        RoutingContext routingContext = new MockedRoutingContext(mapper.serialize(dto), Collections.emptyMap(), response);

        resource.handle(routingContext);

        Metric metric = new Metric(dto.getDeviceId(), dto.getName(), dto.getValue());
        Assert.assertEquals(metricRepository.get(metric.getName()), metric);
        String successResponse = "{\"success\":true,\"content\":\"{\\\"response\\\":true}\"}";
        String actualResponse = routingContext.response().getStatusMessage();
        Assert.assertEquals(successResponse, actualResponse);
    }

    @Test
    public void testOnHandleCounterMetricThenRegisterOnMetricService() {
        MetricDto dto = new MetricDto("aDeviceId", "aName", 0, Type.COUNTER);

        HttpServerResponse response = new MockedHttpServerResponse();
        RoutingContext routingContext = new MockedRoutingContext(mapper.serialize(dto), Collections.emptyMap(), response);

        resource.handle(routingContext);

        Metric metric = new Metric(dto.getDeviceId(), dto.getName(), 1);
        Assert.assertTrue(metricRepository.contains(metric));
        String successResponse = "{\"success\":true,\"content\":\"{\\\"response\\\":true}\"}";
        String actualResponse = routingContext.response().getStatusMessage();
        Assert.assertEquals(successResponse, actualResponse);
    }

    @Test
    public void testOnHandleCounterMetricAlreadyRegisteredThenIncreasePlusOne() {
        MetricDto dto = new MetricDto("aDeviceId", "aName", 0, Type.COUNTER);

        HttpServerResponse response = new MockedHttpServerResponse();
        RoutingContext routingContext = new MockedRoutingContext(mapper.serialize(dto), Collections.emptyMap(), response);

        resource.handle(routingContext);

        Metric metric = new Metric(dto.getDeviceId(), dto.getName(), 2);
        Assert.assertTrue(metricRepository.contains(metric));
        String successResponse = "{\"success\":true,\"content\":\"{\\\"response\\\":true}\"}";
        String actualResponse = routingContext.response().getStatusMessage();
        Assert.assertEquals(successResponse, actualResponse);
    }
}
