package com.moreno.backend.server.rest;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;

public class ResourceResponse {

    public static String createSuccessResponse(Object entity) {
        return new JsonObject().put("success", true)
                               .put("content", Json.encode(entity))
                               .toString();
    }

    public static String createFailureResponse(Object errorEntity) {
        return new JsonObject().put("success", false)
                               .put("content", Json.encode(errorEntity))
                               .toString();
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
