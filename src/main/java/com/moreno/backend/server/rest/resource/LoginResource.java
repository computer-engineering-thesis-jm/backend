package com.moreno.backend.server.rest.resource;

import static com.moreno.backend.server.rest.ResourceResponse.createFailureResponse;
import static com.moreno.backend.server.rest.ResourceResponse.createSuccessResponse;

import org.apache.http.entity.ContentType;
import org.apache.http.protocol.HTTP;

import com.moreno.backend.domain.action.AuthorizeDevice;
import com.moreno.backend.domain.model.AuthorizedDevice;
import com.moreno.backend.server.rest.Mapper;

import io.reactivex.Single;
import io.vertx.core.Handler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;

public class LoginResource implements Handler<RoutingContext> {

    private static final Logger logger = LoggerFactory.getLogger(LoginResource.class);
    private final Mapper mapper;
    private final AuthorizeDevice authorizeDevice;

    public LoginResource(Mapper mapper, AuthorizeDevice authorizeDevice) {
        this.mapper = mapper;
        this.authorizeDevice = authorizeDevice;
    }

    @Override
    public void handle(RoutingContext routingContext) {
        Single.defer(() -> {
            LoginDto dto = mapper.deserialize(routingContext.getBodyAsString(), LoginDto.class);
            String auth = routingContext.request().getHeader("Authorization");
            return authorizeDevice.execute(auth, dto.getDeviceId(), dto.getPassword())
                                  .doOnSuccess(user -> debug(dto))
                                  .doOnError(throwable -> error(dto, throwable));
        }).subscribe(loginResponse -> successResponse(routingContext, loginResponse),
                throwable -> failureResponse(routingContext, throwable));
    }

    private void debug(LoginDto dto) {
        logger.debug("Login device {} with password {}", dto.getDeviceId(), dto.getPassword());
    }

    private void error(LoginDto dto, Throwable throwable) {
        logger.error("Device {} with password {} cannot login", throwable, dto.getDeviceId(), dto.getPassword());
    }

    private void successResponse(RoutingContext routingContext, AuthorizedDevice authorizedDevice) {
        routingContext.response().putHeader(HTTP.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString())
                      .end(createSuccessResponse(authorizedDevice));
    }

    private void failureResponse(RoutingContext routingContext, Throwable throwable) {
        routingContext.response().putHeader(HTTP.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString())
                      .end(createFailureResponse(new ErrorDTO(throwable.getMessage())));
    }

    public static class LoginDto {

        private String deviceId;
        private String password;

        public LoginDto() {
        }

        public LoginDto(String deviceId, String password) {
            this.deviceId = deviceId;
            this.password = password;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public String getPassword() {
            return password;
        }
    }

    public static class ErrorDTO {

        private final String message;

        public ErrorDTO(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
}
