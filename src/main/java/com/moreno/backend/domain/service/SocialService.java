package com.moreno.backend.domain.service;

import java.util.List;

import io.reactivex.Single;

public interface SocialService {
    Single<Boolean> validateItemRequest(String userId, String itemRequestId);

    Single<Boolean> consumeItemRequest(String userId, String itemRequestId);

    Single<Boolean> validateItemRequestContribution(String userId, String itemRequestId, List<String> itemRequestContributionIds);

    Single<Boolean> consumeItemRequestContribution(String userId, String itemRequestId, List<String> itemRequestContributionIds);
}
