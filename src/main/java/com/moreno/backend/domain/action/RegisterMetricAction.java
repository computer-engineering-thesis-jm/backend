package com.moreno.backend.domain.action;

import com.moreno.backend.domain.model.Metric;

public interface RegisterMetricAction {

    void execute(Metric metric);
}
