package com.moreno.backend.infraestructure.metrics;

import java.util.concurrent.TimeUnit;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.Counter;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import com.moreno.backend.domain.MetricRepository;
import com.moreno.backend.domain.model.Metric;
import com.moreno.backend.infraestructure.metrics.provider.GaugeValueProvider;

public class MetricService {

    private static final String NAME = "tesis.moreno.metrics";
    private final MetricRepository metricRepository;
    private final MetricRegistry registry;

    public MetricService(MetricRepository metricRepository, MetricRegistry registry) {
        this.metricRepository = metricRepository;
        this.registry = registry;
        ConsoleReporter.forRegistry(registry)
                       .filter((name, metric) -> name.startsWith(NAME))
                       .convertRatesTo(TimeUnit.SECONDS)
                       .convertDurationsTo(TimeUnit.MILLISECONDS)
                       .build();
    }

    public void gauge(Metric metric, GaugeValueProvider provider) {
        if(metricRepository.contains(metric)) {
            metricRepository.delete(metric.getName());
            registry.remove(metric.getName());
        }
        metricRepository.put(metric);
        registry.register(metric.getName(), (Gauge<Object>) () -> provider.get(metric.getName()));
    }

    public void counter(Metric metric) {
        if (!metricRepository.contains(metric)) {
            registry.counter(metric.getName());
        }

        metricRepository.put(metric);
        Counter counter = registry.counter(metric.getName());
        int newValue = metricRepository.get(metric.getName()).getValue();
        counter.inc(newValue);
        metricRepository.put(new Metric(metric.getDeviceId(), metric.getName(), newValue));
    }

    public Metric get(String name) {
        return metricRepository.get(name);
    }
}
