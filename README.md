# Backend

Java Project to manage the device login, metrics register and other functions associated with the thesis ecosystem.

### Resources

- **Login**: POST /user/login

Body:

```json
{
	"deviceId": "device1",
	"password": "sol1234"
}
```

Success response:

```json
{
	"success": true,
	"content": {
		"deviceId": "device1",
		"password": "sol1234",
		"token": "JsiqNN12-oKn1-nLJNqj2n-9M1k-ONoqwioH"
	}
}
```

Failure response:

```json
{
	"success": false,
	"content": {
		"message": "exception message"
	}
}
```

- **RegisterMetric**: POST /register/metric

Body:

```json
{
	"deviceId" : "device1",
	"name" : "a_metric",
	"type" : "COUNTER | GAUGE",
	"value" : 1
}
```

Success response:

```json
{
	"success": true,
	"content": OK
}
```

- **Prometheus Metrics**: GET /metrics


