package com.moreno.api.server.rest.resource.utils;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.security.cert.X509Certificate;

import io.vertx.core.Handler;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpConnection;
import io.vertx.core.http.HttpFrame;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerFileUpload;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.http.HttpVersion;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.net.NetSocket;
import io.vertx.core.net.SocketAddress;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.Cookie;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Locale;
import io.vertx.ext.web.ParsedHeaderValues;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;

public class MockedRoutingContext implements RoutingContext {

    private final String body;
    private final Map<String, String> headers;
    private final HttpServerResponse response;

    public MockedRoutingContext(String body, Map<String, String> headers, HttpServerResponse response) {
        this.body = body;
        this.headers = headers;
        this.response = response;
    }

    @Override
    public HttpServerRequest request() {
        return new HttpServerRequest() {
            @Override
            public HttpServerRequest exceptionHandler(Handler<Throwable> handler) {
                return null;
            }

            @Override
            public HttpServerRequest handler(Handler<Buffer> handler) {
                return null;
            }

            @Override
            public HttpServerRequest pause() {
                return null;
            }

            @Override
            public HttpServerRequest resume() {
                return null;
            }

            @Override
            public HttpServerRequest endHandler(Handler<Void> endHandler) {
                return null;
            }

            @Override
            public HttpVersion version() {
                return null;
            }

            @Override
            public HttpMethod method() {
                return null;
            }

            @Override
            public String rawMethod() {
                return null;
            }

            @Override
            public boolean isSSL() {
                return false;
            }

            @Override
            public String scheme() {
                return null;
            }

            @Override
            public String uri() {
                return null;
            }

            @Override
            public String path() {
                return null;
            }

            @Override
            public String query() {
                return null;
            }

            @Override
            public String host() {
                return null;
            }

            @Override
            public HttpServerResponse response() {
                return null;
            }

            @Override
            public MultiMap headers() {
                return null;
            }

            @Override
            public String getHeader(String headerName) {
                return headers.get(headerName);
            }

            @Override
            public String getHeader(CharSequence headerName) {
                return null;
            }

            @Override
            public MultiMap params() {
                return null;
            }

            @Override
            public String getParam(String paramName) {
                return null;
            }

            @Override
            public SocketAddress remoteAddress() {
                return null;
            }

            @Override
            public SocketAddress localAddress() {
                return null;
            }

            @Override
            public SSLSession sslSession() {
                return null;
            }

            @Override
            public X509Certificate[] peerCertificateChain() throws SSLPeerUnverifiedException {
                return new X509Certificate[0];
            }

            @Override
            public String absoluteURI() {
                return null;
            }

            @Override
            public NetSocket netSocket() {
                return null;
            }

            @Override
            public HttpServerRequest setExpectMultipart(boolean expect) {
                return null;
            }

            @Override
            public boolean isExpectMultipart() {
                return false;
            }

            @Override
            public HttpServerRequest uploadHandler(Handler<HttpServerFileUpload> uploadHandler) {
                return null;
            }

            @Override
            public MultiMap formAttributes() {
                return null;
            }

            @Override
            public String getFormAttribute(String attributeName) {
                return null;
            }

            @Override
            public ServerWebSocket upgrade() {
                return null;
            }

            @Override
            public boolean isEnded() {
                return false;
            }

            @Override
            public HttpServerRequest customFrameHandler(Handler<HttpFrame> handler) {
                return null;
            }

            @Override
            public HttpConnection connection() {
                return null;
            }
        };
    }

    @Override
    public HttpServerResponse response() {
        return response;
    }

    @Override
    public void next() {

    }

    @Override
    public void fail(int i) {

    }

    @Override
    public void fail(Throwable throwable) {

    }

    @Override
    public RoutingContext put(String s, Object o) {
        return null;
    }

    @Override
    public <T> T get(String s) {
        return null;
    }

    @Override
    public <T> T remove(String s) {
        return null;
    }

    @Override
    public Map<String, Object> data() {
        return null;
    }

    @Override
    public Vertx vertx() {
        return null;
    }

    @Override
    public String mountPoint() {
        return null;
    }

    @Override
    public Route currentRoute() {
        return null;
    }

    @Override
    public String normalisedPath() {
        return null;
    }

    @Override
    public Cookie getCookie(String s) {
        return null;
    }

    @Override
    public RoutingContext addCookie(Cookie cookie) {
        return null;
    }

    @Override
    public Cookie removeCookie(String s) {
        return null;
    }

    @Override
    public int cookieCount() {
        return 0;
    }

    @Override
    public Set<Cookie> cookies() {
        return null;
    }

    @Override
    public String getBodyAsString() {
        return body;
    }

    @Override
    public String getBodyAsString(String s) {
        return null;
    }

    @Override
    public JsonObject getBodyAsJson() {
        return null;
    }

    @Override
    public JsonArray getBodyAsJsonArray() {
        return null;
    }

    @Override
    public Buffer getBody() {
        return null;
    }

    @Override
    public Set<FileUpload> fileUploads() {
        return null;
    }

    @Override
    public Session session() {
        return null;
    }

    @Override
    public User user() {
        return null;
    }

    @Override
    public Throwable failure() {
        return null;
    }

    @Override
    public int statusCode() {
        return 0;
    }

    @Override
    public String getAcceptableContentType() {
        return null;
    }

    @Override
    public ParsedHeaderValues parsedHeaders() {
        return null;
    }

    @Override
    public int addHeadersEndHandler(Handler<Void> handler) {
        return 0;
    }

    @Override
    public boolean removeHeadersEndHandler(int i) {
        return false;
    }

    @Override
    public int addBodyEndHandler(Handler<Void> handler) {
        return 0;
    }

    @Override
    public boolean removeBodyEndHandler(int i) {
        return false;
    }

    @Override
    public boolean failed() {
        return false;
    }

    @Override
    public void setBody(Buffer buffer) {

    }

    @Override
    public void setSession(Session session) {

    }

    @Override
    public void setUser(User user) {

    }

    @Override
    public void clearUser() {

    }

    @Override
    public void setAcceptableContentType(String s) {

    }

    @Override
    public void reroute(HttpMethod httpMethod, String s) {

    }

    @Override
    public List<Locale> acceptableLocales() {
        return null;
    }

    @Override
    public Map<String, String> pathParams() {
        return null;
    }

    @Override
    public String pathParam(String s) {
        return null;
    }

    @Override
    public MultiMap queryParams() {
        return null;
    }

    @Override
    public List<String> queryParam(String s) {
        return null;
    }
}
