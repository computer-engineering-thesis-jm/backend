package com.moreno.api.server.rest.resource.utils;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.MultiMap;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;

public class MockedHttpServerResponse implements HttpServerResponse {

    private String message;

    public MockedHttpServerResponse() {
        this.message = "";
    }

    @Override
    public HttpServerResponse exceptionHandler(Handler<Throwable> handler) {
        return null;
    }

    @Override
    public HttpServerResponse write(Buffer buffer) {
        return null;
    }

    @Override
    public HttpServerResponse setWriteQueueMaxSize(int i) {
        return null;
    }

    @Override
    public boolean writeQueueFull() {
        return false;
    }

    @Override
    public HttpServerResponse drainHandler(Handler<Void> handler) {
        return null;
    }

    @Override
    public int getStatusCode() {
        return 0;
    }

    @Override
    public HttpServerResponse setStatusCode(int i) {
        return null;
    }

    @Override
    public String getStatusMessage() {
        return message;
    }

    @Override
    public HttpServerResponse setStatusMessage(String s) {
        message = s;
        return this;
    }

    @Override
    public HttpServerResponse setChunked(boolean b) {
        return null;
    }

    @Override
    public boolean isChunked() {
        return false;
    }

    @Override
    public MultiMap headers() {
        return null;
    }

    @Override
    public HttpServerResponse putHeader(String s, String s1) {
        return this;
    }

    @Override
    public HttpServerResponse putHeader(CharSequence charSequence, CharSequence charSequence1) {
        return null;
    }

    @Override
    public HttpServerResponse putHeader(String s, Iterable<String> iterable) {
        return null;
    }

    @Override
    public HttpServerResponse putHeader(CharSequence charSequence, Iterable<CharSequence> iterable) {
        return null;
    }

    @Override
    public MultiMap trailers() {
        return null;
    }

    @Override
    public HttpServerResponse putTrailer(String s, String s1) {
        return null;
    }

    @Override
    public HttpServerResponse putTrailer(CharSequence charSequence, CharSequence charSequence1) {
        return null;
    }

    @Override
    public HttpServerResponse putTrailer(String s, Iterable<String> iterable) {
        return null;
    }

    @Override
    public HttpServerResponse putTrailer(CharSequence charSequence, Iterable<CharSequence> iterable) {
        return null;
    }

    @Override
    public HttpServerResponse closeHandler(Handler<Void> handler) {
        return null;
    }

    @Override
    public HttpServerResponse endHandler(Handler<Void> handler) {
        return null;
    }

    @Override
    public HttpServerResponse write(String s, String s1) {
        return null;
    }

    @Override
    public HttpServerResponse write(String s) {
        return null;
    }

    @Override
    public HttpServerResponse writeContinue() {
        return null;
    }

    @Override
    public void end(String s) {
        this.message = s;
    }

    @Override
    public void end(String s, String s1) {

    }

    @Override
    public void end(Buffer buffer) {

    }

    @Override
    public void end() {

    }

    @Override
    public HttpServerResponse sendFile(String s, long l, long l1) {
        return null;
    }

    @Override
    public HttpServerResponse sendFile(String s, long l, long l1, Handler<AsyncResult<Void>> handler) {
        return null;
    }

    @Override
    public void close() {

    }

    @Override
    public boolean ended() {
        return false;
    }

    @Override
    public boolean closed() {
        return false;
    }

    @Override
    public boolean headWritten() {
        return false;
    }

    @Override
    public HttpServerResponse headersEndHandler(Handler<Void> handler) {
        return null;
    }

    @Override
    public HttpServerResponse bodyEndHandler(Handler<Void> handler) {
        return null;
    }

    @Override
    public long bytesWritten() {
        return 0;
    }

    @Override
    public int streamId() {
        return 0;
    }

    @Override
    public HttpServerResponse push(HttpMethod httpMethod, String s, String s1, Handler<AsyncResult<HttpServerResponse>> handler) {
        return null;
    }

    @Override
    public HttpServerResponse push(HttpMethod httpMethod, String s, MultiMap multiMap, Handler<AsyncResult<HttpServerResponse>> handler) {
        return null;
    }

    @Override
    public HttpServerResponse push(HttpMethod httpMethod, String s, Handler<AsyncResult<HttpServerResponse>> handler) {
        return null;
    }

    @Override
    public HttpServerResponse push(HttpMethod httpMethod, String s, String s1, MultiMap multiMap,
            Handler<AsyncResult<HttpServerResponse>> handler) {
        return null;
    }

    @Override
    public void reset(long l) {

    }

    @Override
    public HttpServerResponse writeCustomFrame(int i, int i1, Buffer buffer) {
        return null;
    }
}
