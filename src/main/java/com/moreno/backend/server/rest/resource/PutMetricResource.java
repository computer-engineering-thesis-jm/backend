package com.moreno.backend.server.rest.resource;

import static com.moreno.backend.domain.model.Metric.Type;
import static com.moreno.backend.server.rest.ResourceResponse.createSuccessResponse;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.http.entity.ContentType;
import org.apache.http.protocol.HTTP;

import com.moreno.backend.domain.action.RegisterMetric;
import com.moreno.backend.server.rest.Mapper;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.vertx.core.Handler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;

public class PutMetricResource implements Handler<RoutingContext> {

    private static final Logger logger = LoggerFactory.getLogger(PutMetricResource.class);

    private final RegisterMetric registerMetric;
    private final Mapper mapper;

    public PutMetricResource(Mapper mapper, RegisterMetric registerMetric) {
        this.mapper = mapper;
        this.registerMetric = registerMetric;
    }

    @Override
    public void handle(RoutingContext routingContext) {
        Single.fromCallable(() -> mapper.deserialize(routingContext.getBodyAsString(), MetricDto.class))
              .flatMapCompletable(this::registerMetric)
              .subscribe(() -> successResponse(routingContext), t -> failureResponse(routingContext));
    }

    private Completable registerMetric(MetricDto metricDto) {
        return registerMetric.execute(metricDto)
                             .doOnComplete(() -> logger.debug("Metric {} registered", metricDto));
    }

    private void successResponse(RoutingContext routingContext) {
        routingContext.response().putHeader(HTTP.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString())
                      .end(createSuccessResponse(Response.success()));
    }

    private void failureResponse(RoutingContext routingContext) {
        routingContext.response().putHeader(HTTP.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString())
                      .end(createSuccessResponse(Response.failure()));
    }

    public static class MetricDto {

        private String deviceId;
        private String name;
        private Integer value;
        private Type type;

        public MetricDto() {
        }

        public MetricDto(String deviceId, String name, Integer value, Type type) {
            this.deviceId = deviceId;
            this.name = name;
            this.value = value;
            this.type = type;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public String getName() {
            return name;
        }

        public Integer getValue() {
            return value;
        }

        public Type getType() {
            return type;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }

    public static class Response {

        private boolean response;

        public Response() {
        }

        public Response(boolean response) {
            this.response = response;
        }

        public static Response success() {
            return new Response(true);
        }

        public static Response failure() {
            return new Response(false);
        }

        public boolean getResponse() {
            return response;
        }
    }
}
