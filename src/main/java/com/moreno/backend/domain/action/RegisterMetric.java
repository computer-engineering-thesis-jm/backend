package com.moreno.backend.domain.action;

import java.util.HashMap;
import java.util.Map;

import com.moreno.backend.domain.model.Metric;
import com.moreno.backend.server.rest.resource.PutMetricResource;

import io.reactivex.Completable;

public class RegisterMetric {

    private final Map<Metric.Type, RegisterMetricAction> map;

    public RegisterMetric(RegisterGaugeMetric registerGaugeMetric, RegisterCounterMetric registerCounterMetric) {
        this.map = new HashMap<>();
        this.map.put(Metric.Type.GAUGE, registerGaugeMetric);
        this.map.put(Metric.Type.COUNTER, registerCounterMetric);
    }

    public Completable execute(PutMetricResource.MetricDto metricDto) {
        return Completable.fromAction(() -> {
            if (map.containsKey(metricDto.getType())) {
                map.get(metricDto.getType()).execute(toMetric(metricDto));
            }
        });
    }

    private Metric toMetric(PutMetricResource.MetricDto metricDto) {
        return new Metric(metricDto.getDeviceId(), metricDto.getName(), metricDto.getValue());
    }
}
