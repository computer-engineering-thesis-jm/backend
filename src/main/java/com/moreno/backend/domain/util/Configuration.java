package com.moreno.backend.domain.util;

import static java.lang.System.getenv;

import java.util.Optional;

public class Configuration {

    public String getAuthorizationKey() {
        return Optional.ofNullable(getenv("AUTHORIZATION")).orElse("secret");
    }

    public int getApiPort() {
        return Integer.parseInt(Optional.ofNullable(getenv("API_PORT")).orElse("8080"));
    }
}
