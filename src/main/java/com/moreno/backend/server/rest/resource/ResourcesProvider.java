package com.moreno.backend.server.rest.resource;

import static com.moreno.backend.InstanceCache.getInstanceFor;

import com.moreno.backend.domain.action.ActionsProvider;
import com.moreno.backend.server.rest.Mapper;

import io.prometheus.client.vertx.MetricsHandler;

public class ResourcesProvider {

    public static LoginResource provideLoginResource(String authorization) {
        return getInstanceFor(LoginResource.class, () -> new LoginResource(getMapper(), ActionsProvider.getAuthorizeDevice(authorization)));
    }

    public static PutMetricResource providePutMetricResource() {
        return getInstanceFor(PutMetricResource.class, () -> new PutMetricResource(getMapper(), ActionsProvider.getRegisterMetric()));
    }

    public static MetricsHandler provideMetricsResource() {
        return getInstanceFor(MetricsHandler.class, () -> new MetricsHandler());
    }

    private static Mapper getMapper() {
        return getInstanceFor(Mapper.class, () -> new Mapper());
    }
}

