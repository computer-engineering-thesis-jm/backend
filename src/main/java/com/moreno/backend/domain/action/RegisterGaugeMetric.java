package com.moreno.backend.domain.action;

import com.moreno.backend.domain.model.Metric;
import com.moreno.backend.infraestructure.metrics.MetricService;
import com.moreno.backend.infraestructure.metrics.provider.GaugeValueProvider;

public class RegisterGaugeMetric implements GaugeValueProvider<Integer>, RegisterMetricAction {

    private final MetricService metricService;

    public RegisterGaugeMetric(MetricService metricService) {
        this.metricService = metricService;
    }

    public void execute(Metric metric) {
        metricService.gauge(metric, this);
    }

    @Override
    public Integer get(String name) {
        return metricService.get(name).getValue();
    }
}
