package com.moreno.backend.domain;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.moreno.backend.domain.model.Metric;

public class MetricRepository {

    private final Map<String, Metric> metrics;

    public MetricRepository() {
        this.metrics = new ConcurrentHashMap<>();
    }

    public Metric get(String name) {
        return metrics.get(name);
    }

    public void put(Metric metric) {
        metrics.put(metric.getName(), metric);
    }

    public boolean contains(Metric metric) {
        return metrics.containsKey(metric.getName());
    }

    public void delete(String name) {
        metrics.remove(name);
    }
}
